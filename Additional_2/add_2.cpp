﻿#include<iostream> 
#include<ctime>
#include<cmath>
#include <algorithm>
#include <conio.h>
#include <chrono>
#define N 50
int a[N];
int b[N];
using namespace std;
void int_massiv()
{
	// определяем переменные

	int i;

	srand(time(0)); // инициализация генерации случайных чисел

	//генерируем целый случайный массив из 10 эелментов от 1 до 5
	for (i = 0; i < N - 1; i++) {
		a[i] = 1 + rand() % 60;
		//a[i] = i;
		b[i] = a[i];
		cout << a[i] << endl;
	}
}

int ShellSort(int n, int x[]) {
    
    for (int gap = n / 2; gap > 0; gap /= 2)
    {     
        for (int i = gap; i < n; i += 1)
        {
            int temp = x[i];
            int j;
            for (j = i; j >= gap && x[j - gap] > temp; j -= gap)
                x[j] = x[j - gap];
            x[j] = temp;
        }
    }
    return 0;
}
void printArray(int x[], int n) {
	for (int i = 0; i < n; i++) {
		cout << x[i] << endl;
	}
}

int main() {
	setlocale(LC_ALL, "Russian");
	cout << "---Не отсортированная последовательность-----" << endl;
	int_massiv();
	ShellSort(N, b);
	cout << "---Отсортированная последовательность-----" << endl;
	printArray(b, N);



}