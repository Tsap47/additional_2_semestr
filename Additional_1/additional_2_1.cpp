﻿/*Вариант 5.
Дано натуральное число N < 10 ^ 6. Выведите его наоборот.Например, 123 превратится в 321. 
 Важно, чтобы на выходе у вас было число.Т.е.выводить цифры сразу в консоль нельзя.
    */

#include <iostream>
using namespace std;

void Chislo(int i) {
    int t, d = 0;
    if (i == 0) {
        return;
    }
    d += 10 * d + i % 10;
    t = i / 10;
    cout << d;
    Chislo(t);
}

int main()
{
    setlocale(LC_ALL, "Russian");
    int n;
    cout << "Введите число до 10^6. Оно выведится наоборот с помощью рекурсии" << endl;
    cin >> n;
    Chislo(n);
}
