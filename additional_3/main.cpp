#include <iostream>

struct list
{
    int field; // ���� ������
    struct list* next; // ��������� �� ��������� �������
    struct list* prev; // ��������� �� ���������� �������
};

struct list* init(int a)  // �- �������� ������� ����
{
    struct list* lst;
    // ��������� ������ ��� ������ ������
    lst = (struct list*)malloc(sizeof(struct list));
    lst->field = a;
    lst->next = lst; // ��������� �� ��������� ����
    lst->prev = lst; // ��������� �� ���������� ����
    return(lst);
}

struct list* Search(list* lst, int a)
{
    struct list* tmp, p;
    tmp = lst->next;
    if (tmp == lst)
        return lst;
    else
        while (tmp != lst)
        {
            if (tmp->field == a)
                return tmp;
            tmp = tmp->next;
        }
}

struct list* addelem(list* lst, int number)
{
    struct list* temp, * p;
    temp = (struct list*)malloc(sizeof(list));
    p = lst->next; // ���������� ��������� �� ��������� ����
    lst->next = temp; // ���������� ���� ��������� �� �����������
    temp->field = number; // ���������� ���� ������ ������������ ����
    temp->next = p; // ��������� ���� ��������� �� ��������� ����
    temp->prev = lst; // ��������� ���� ��������� �� ���������� ����
    p->prev = temp;
    return(temp);
}

struct list* deletelem(list* lst)
{
    struct list* prev, * next;
    prev = lst->prev; // ����, �������������� lst
    next = lst->next; // ����, ��������� �� lst
    prev->next = lst->next; // ������������ ���������
    next->prev = lst->prev; // ������������ ���������
    free(lst); // ����������� ������ ���������� ��������
    return(prev);
}

void listprint(list* lst)
{
    struct list* p;
    p = lst;
    do {
        printf("%d ", p->field); // ����� �������� �������� p
        p = p->next; // ������� � ���������� ����
    } while (p != lst); // ������� ��������� ������
}

void listprintr(list *lst)
{
  struct list *p;
  p = lst;
  do {
    p = p->prev;  // ������� � ����������� ����
    printf("%d ", p->field); // ����� �������� �������� p
  } while (p != lst); // ������� ��������� ������
}

int main() {
    list* lst = init(5);
    addelem(lst, 10);
    addelem(lst, 15);
    addelem(lst, 9);

    listprint(lst);
    std::cout << std::endl;

    deletelem(Search(lst,10));
    listprint(lst);
    std::cout << std::endl;

    listprintr(lst);


    return 0;
}